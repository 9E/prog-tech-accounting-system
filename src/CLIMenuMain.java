import accounting.commandlineui.CommandLineMenu;

public class CLIMenuMain {


    public static void main(String[] args) {
        startSystem();
    }

    static void startSystem() {
        CommandLineMenu commandLineMenu = new CommandLineMenu();
        commandLineMenu.welcomePrompt();
    }
}
