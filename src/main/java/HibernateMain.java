import accounting.mvcjavafx.gson.*;
import accounting.mvcjavafx.hibernate.AccountingSystemHibernateControl;
import accounting.mvcjavafx.hibernate.CategoryHibernateControl;
import accounting.mvcjavafx.hibernate.IncomeHibernateControl;
import accounting.mvcjavafx.hibernate.UserHibernateControl;
import accounting.mvcjavafx.model.*;
import accounting.mvcjavafx.model.accsys.AccountingSystem;
import accounting.mvcjavafx.model.accsys.VanillaAccountingSystem;
import accounting.mvcjavafx.model.moneyflow.Expense;
import accounting.mvcjavafx.model.moneyflow.Income;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.time.LocalDate;
import java.util.Properties;

public class HibernateMain {
    static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory(Constants.SCHEMA_NAME);
    static UserHibernateControl userHibernateControl = new UserHibernateControl(entityManagerFactory);
    static CategoryHibernateControl categoryHibernateControl = new CategoryHibernateControl(entityManagerFactory);
    static AccountingSystemHibernateControl accountingSystemHibernateControl =
            new AccountingSystemHibernateControl(entityManagerFactory);

    public static void main(String[] args) {
//        initializeSchemaWithData();

//        testUserForeignKeyInsert();
//        testUserForeignKeyDelete();
//        addUserWithContactInfo();
//        accountingSystemTest();
//        testAddingCategoryToUser();
//        testGson();
//        System.out.println(categoryHibernateControl.getAllParentCategories());
//        testDeletingCategory();
//        testDeleteUser();
//        testDeletingCategory();
//        System.out.println(accountingSystemHibernateControl.getAccountingSystemById(1L).getSystemRootCategories());

//        testAddingParentCategory();
//        testAddingExpenseWithCategory();

//        User user = userHibernateControl.getUserByUsername("1");
//        for (Category category : user.getSupervisedCategories()) {
//            System.out.println(category);
//        }

//        testWebUpdateUser();
//        testWebUpdateIncome();
        testFromJson();
    }

    public static void testFromJson() {
//        String date = "2020-10-11";
//        LocalDate date = LocalDate.parse("date");
//        System.out.println(date);

//        String userString = "{\n" +
//                "    \"id\": 1,\n" +
//                "    \"username\": \"1\",\n" +
//                "    \"password\": \"\",\n" +
//                "    \"creationDate\": \"2020-11-24\",\n" +
//                "    \"firstName\": \"\",\n" +
//                "    \"lastName\": \"\"\n" +
//                "}";
//
//        Gson gson = new Gson();
//        User user = gson.fromJson(userString, Person.class);
//        System.out.println(user);
    }

    public static void testWebUpdateUser(){
//        String request = "{\"id\":1,\"username\":\"1\",\"password\":\"\",\"contactInfo\":{\"id\":1,\"address\":\"House st.\",\"email\":\"the@email.com\",\"phoneNumber\":\"987654321\"}}";
//        String request = "{\"id\":1,\"username\":\"1\",\"password\":\"\"}";
        String request = "{\"id\":1,\"username\":\"1\",\"password\":\"\",\"nested\":{\"a\":1,\"b\":2}}";

        Gson parser = new Gson();
        Properties data = parser.fromJson(request, Properties.class);
        String id = data.getProperty("id");
        String username = data.getProperty("username");
        String password = data.getProperty("password");
//        String contactInfoProperty = data.getProperty("contactInfo");
//        System.out.println(contactInfoProperty);
//        ContactInfo contactInfo = parser.fromJson(data.getProperty("contactInfo"), ContactInfo.class);
//        User user = userHibernateControl.getUserById(Long.parseLong(id));
//        user.setUsername(username);
//        user.setPassword(password);
//        user.setContactInfo(contactInfo);
//        userHibernateControl.edit(user);
    }

    public static void testWebUpdateIncome() {
        IncomeHibernateControl incomeHibernateControl = new IncomeHibernateControl(entityManagerFactory);
        String request = "{\n" +
                "    \"id\": 4,\n" +
                "    \"amount\": 25,\n" +
                "    \"name\": \"mr income\"\n" +
                "}";

        Gson parser = new Gson();
        Properties data = parser.fromJson(request, Properties.class);
        String id = data.getProperty("id");
        String name = data.getProperty("name");
        String amount = data.getProperty("amount");
        Income income = incomeHibernateControl.getIncomeById(Long.parseLong(id));
        income.setName(name);
        income.setAmount(Double.parseDouble(amount));
        incomeHibernateControl.edit(income);
    }

    public static void testGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Income.class, new IncomeJsonSerializer());
        gsonBuilder.registerTypeAdapter(Expense.class, new ExpenseJsonSerializer());
        gsonBuilder.registerTypeAdapter(Category.class, new CategoryJsonSerializer());
        gsonBuilder.registerTypeAdapter(Company.class, new CompanyJsonSerializer());
        gsonBuilder.registerTypeAdapter(Person.class, new PersonJsonSerializer());
        Gson gson = gsonBuilder.create();

//        System.out.println(gson.toJson(userHibernateControl.getUserById(1L)));
        System.out.println(gson.toJson(categoryHibernateControl.getAllRootCategories()));
    }

    public static void testAddingExpenseWithCategory() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        Category category = entityManager.find(Category.class, 5L);
        Expense expense = new Expense("exp name", category.getCreator(), 100., "123");
        expense.setCategory(category);
        category.getExpenses().add(expense);

        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public static void testAddingParentCategory() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        User user = entityManager.find(User.class, 1L);
        Category category = new Category(user, null, "parent category");
        user.getSupervisedCategories().add(category);

        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public static void testDeletingCategory() {
//        Category category = categoryHibernateControl.getCategoryById(12L);

        categoryHibernateControl.remove(2L);

//        EntityManager entityManager = entityManagerFactory.createEntityManager();
//        entityManager.getTransaction().begin();
//
//        Category category = entityManager.find(Category.class, 1L);
//
//        if (category.hasParentCategory()) {
//            User creator = category.getCreator();
//            Category parentCategory = category.getParentCategory();
//            AccountingSystem accountingSystem = category.getAccountingSystem();
//
////            accountingSystem.getSystemRootCategories().remove();
//            parentCategory.getSubcategories().remove(category);
//        } else {
//            User creator = category.getCreator();
//            AccountingSystem accountingSystem = category.getAccountingSystem();
//
//            category.getSupervisors().remove(creator);
//            creator.getSupervisedCategories().remove(category);
//            accountingSystem.getSystemRootCategories().remove(category);
//        }
//
//        entityManager.getTransaction().commit();
//        entityManager.close();
    }

    public static void testAddingCategoryToUser() {
        User user = userHibernateControl.getUserByUsername("nameuser4");

        user.addParentCategory("some name");

        userHibernateControl.edit(user);
    }

    public static void testUserForeignKeyInsert() {
        AccountingSystem accountingSystem = accountingSystemHibernateControl.getAccountingSystemById(2L);
        System.out.println(accountingSystem);

        ContactInfo contactInfo = new ContactInfo("phone", "email", "address");
        User person = new Person("nameuser4", "wordpass", contactInfo, "first", "last", accountingSystem);
        userHibernateControl.create(person);
    }

    public static void testDeleteUser() {
        userHibernateControl.remove(1L);
    }

    public static void initializeSchemaWithData() {
        AccountingSystem accountingSystem = new VanillaAccountingSystem("NameSystemAccounting", "0.99");
        ContactInfo contactInfo = new ContactInfo("phone", "email", "address");
        User person = new Person("nameuser4", "wordpass", contactInfo, "first", "last", accountingSystem);

        accountingSystem.getSystemUsers().add(person);
        accountingSystemHibernateControl.create(accountingSystem);
    }

    private static void addUserWithContactInfo() {
        ContactInfo contactInfo = new ContactInfo("phone", "email", "address");
        User user = new Person("nameuser1", "wordpass", contactInfo, "first", "last");
        userHibernateControl.create(user);
    }

    private static void accountingSystemTest() {
        AccountingSystem accountingSystem = new VanillaAccountingSystem("NameSystemAccounting", "0.99");


        ContactInfo contactInfo = new ContactInfo("phone", "email", "address");
        User person = new Person("nameuser2", "wordpass", contactInfo, "first", "last");

        contactInfo = new ContactInfo("phone", "email", "address");
        User company = new Company("nameuser3", "wordpass", contactInfo, "namecompany");

        person.addParentCategory("parentCat"); //
        person.addParentCategory("cat2parent");

        Category parentCat = person.getSupervisedCategories().get(1);
        Category childCat = new Category(person, parentCat, "childCat");
        parentCat.addSubcategory(childCat);

        person.assignCategoryTo(company, parentCat);

        accountingSystem.getSystemUsers().add(person);
        accountingSystem.getSystemUsers().add(company);

        accountingSystemHibernateControl.create(accountingSystem);
    }
}
