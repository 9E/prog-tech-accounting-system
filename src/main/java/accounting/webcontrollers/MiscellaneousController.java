package accounting.webcontrollers;

import accounting.mvcjavafx.gson.CategoryJsonSerializer;
import accounting.mvcjavafx.gson.CompanyJsonSerializer;
import accounting.mvcjavafx.gson.IncomeJsonSerializer;
import accounting.mvcjavafx.gson.PersonJsonSerializer;
import accounting.mvcjavafx.hibernate.ExpenseHibernateControl;
import accounting.mvcjavafx.hibernate.IncomeHibernateControl;
import accounting.mvcjavafx.model.Category;
import accounting.mvcjavafx.model.Company;
import accounting.mvcjavafx.model.Constants;
import accounting.mvcjavafx.model.Person;
import accounting.mvcjavafx.model.moneyflow.Income;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Properties;

@Controller
public class MiscellaneousController {

    EntityManagerFactory entityManagerFactory;
    ExpenseHibernateControl expenseHibernateControl;
    IncomeHibernateControl incomeHibernateControl;
    Gson gson;

    {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Income.class, new IncomeJsonSerializer());
        gsonBuilder.registerTypeAdapter(Income.class, new IncomeJsonSerializer());
        gsonBuilder.registerTypeAdapter(Category.class, new CategoryJsonSerializer());
        gsonBuilder.registerTypeAdapter(Company.class, new CompanyJsonSerializer());
        gsonBuilder.registerTypeAdapter(Person.class, new PersonJsonSerializer());

        this.gson = gsonBuilder.create();
        this.entityManagerFactory = Persistence.createEntityManagerFactory(Constants.SCHEMA_NAME);
        this.incomeHibernateControl = new IncomeHibernateControl(entityManagerFactory);
        this.expenseHibernateControl = new ExpenseHibernateControl(entityManagerFactory);
    }

    @RequestMapping(value = "misc/getTotalBalanceInfoByDate", method = RequestMethod.GET)
    @ResponseBody
    public String getTotalIncomeByDate(@RequestParam String fromDate,@RequestParam String toDate) {
        LocalDate localDateFromDate;
        LocalDate localDateToDate;

        try {
            localDateFromDate = (fromDate == null || "".equals(fromDate)) ? null : LocalDate.parse(fromDate);
        } catch (DateTimeParseException exception) {
            return "Failed while parsing fromDate";
        }

        try {
            localDateToDate = (toDate == null || "".equals(toDate)) ? null : LocalDate.parse(toDate);
        } catch (DateTimeParseException exception) {
            return "Failed while parsing toDate";
        }

        double totalIncome = incomeHibernateControl.getTotalIncomeByDate(localDateFromDate, localDateToDate);
        double totalExpenses = expenseHibernateControl.getTotalExpensesByDate(localDateFromDate, localDateToDate);
        double totalBalance = totalIncome - totalExpenses;
        System.out.println(totalBalance);

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("totalIncome", totalIncome);
        jsonObject.addProperty("totalExpenses", totalExpenses);
        jsonObject.addProperty("totalBalance", totalBalance);

        return gson.toJson(jsonObject);
    }
}
