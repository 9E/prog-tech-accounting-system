package accounting.webcontrollers;

import accounting.mvcjavafx.gson.*;
import accounting.mvcjavafx.hibernate.CategoryHibernateControl;
import accounting.mvcjavafx.hibernate.ExpenseHibernateControl;
import accounting.mvcjavafx.hibernate.IncomeHibernateControl;
import accounting.mvcjavafx.model.Category;
import accounting.mvcjavafx.model.Company;
import accounting.mvcjavafx.model.Constants;
import accounting.mvcjavafx.model.Person;
import accounting.mvcjavafx.model.moneyflow.Expense;
import accounting.mvcjavafx.model.moneyflow.Income;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Properties;

@Controller
public class ExpenseWebController {


    EntityManagerFactory entityManagerFactory;
    ExpenseHibernateControl expenseHibernateControl;
    Gson gson;

    {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Income.class, new IncomeJsonSerializer());
        gsonBuilder.registerTypeAdapter(Expense.class, new ExpenseJsonSerializer());
        gsonBuilder.registerTypeAdapter(Category.class, new CategoryJsonSerializer());
        gsonBuilder.registerTypeAdapter(Company.class, new CompanyJsonSerializer());
        gsonBuilder.registerTypeAdapter(Person.class, new PersonJsonSerializer());

        this.gson = gsonBuilder.create();
        this.entityManagerFactory = Persistence.createEntityManagerFactory(Constants.SCHEMA_NAME);
        this.expenseHibernateControl = new ExpenseHibernateControl(entityManagerFactory);
    }

    @RequestMapping(value = "expense/getTotalExpensesByDate", method = RequestMethod.GET)
    @ResponseBody
    public String getTotalIncomeByDate(String fromDate, String toDate) {
        LocalDate localDateFromDate;
        LocalDate localDateToDate;

        try {
            localDateFromDate = (fromDate == null || "".equals(fromDate)) ? null : LocalDate.parse(fromDate);
        } catch (DateTimeParseException exception) {
            return "Failed while parsing fromDate";
        }

        try {
            localDateToDate = (toDate == null || "".equals(toDate)) ? null : LocalDate.parse(toDate);
        } catch (DateTimeParseException exception) {
            return "Failed while parsing toDate";
        }

        double totalIncome = expenseHibernateControl.getTotalExpensesByDate(localDateFromDate, localDateToDate);
        return gson.toJson(totalIncome);
    }

    @RequestMapping(value = "expense/getAllExpensesOfCategory", method = RequestMethod.GET)
    @ResponseBody
    public String getAllExpensesOfCategory(Long categoryId){
        List<Expense> expenses = expenseHibernateControl.getAllExpensesOfCategory(categoryId);
        return gson.toJson(expenses);
    }

    @RequestMapping(value = "expense/getExpenseById", method = RequestMethod.GET)
    @ResponseBody
    public String getExpenseById(Long id){
        Expense expense = expenseHibernateControl.getExpenseById(id);
        return gson.toJson(expense);
    }

    @RequestMapping(value = "expense/updateExpense", method = RequestMethod.PUT)
    @ResponseBody
    public String updateExpense(@RequestBody String request){
        Gson parser = new Gson();
        Properties data = parser.fromJson(request, Properties.class);
        String id = data.getProperty("expenseId");
        String name = data.getProperty("expenseName");
        String amount = data.getProperty("expenseAmount");
        String receiptNumber = data.getProperty("expenseReceiptNumber");
        Expense expense = expenseHibernateControl.getExpenseById(Long.parseLong(id));
        expense.setName(name);
        expense.setAmount(Double.parseDouble(amount));
        expense.setReceiptNumber(receiptNumber);
        expenseHibernateControl.edit(expense);
        return "Expense updated";
    }

    @RequestMapping(value = "expense/deleteExpense", method = RequestMethod.DELETE)
    @ResponseBody
    public String deleteExpense(@RequestBody String request){
        Properties data = new Gson().fromJson(request, Properties.class);
        Long id = Long.parseLong(data.getProperty("id"));
        if (expenseHibernateControl.getExpenseById(id) == null) {
            return "Expense doesn't exist";
        }
        expenseHibernateControl.remove(id);
        return "Expense deleted";
    }

    @RequestMapping(value = "expense/createExpense", method = RequestMethod.POST)
    @ResponseBody
    public String createExpense(@RequestBody String request){
        Properties data = new Gson().fromJson(request, Properties.class);
        Long categoryId = Long.parseLong(data.getProperty("categoryId"));

        CategoryHibernateControl categoryHibernateControl = new CategoryHibernateControl(entityManagerFactory);
        if (categoryHibernateControl.getCategoryById(categoryId) == null) {
            return "Category doesn't exist. Expense creation failed.";
        }

        String expenseName = data.getProperty("expenseName");
        Double expenseAmount = Double.parseDouble(data.getProperty("expenseAmount"));
        String expenseReceiptNumber = data.getProperty("expenseReceiptNumber");

        Expense expense = new Expense(expenseName, expenseAmount, expenseReceiptNumber);
        expenseHibernateControl.createExpense(expense, categoryId);
        return "Expense created";
    }
}
