package accounting.webcontrollers;

import accounting.mvcjavafx.gson.*;
import accounting.mvcjavafx.hibernate.CategoryHibernateControl;
import accounting.mvcjavafx.hibernate.IncomeHibernateControl;
import accounting.mvcjavafx.hibernate.UserHibernateControl;
import accounting.mvcjavafx.model.*;
import accounting.mvcjavafx.model.moneyflow.Income;
import accounting.mvcjavafx.model.moneyflow.Income;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Properties;

@Controller
public class IncomeWebController {


    EntityManagerFactory entityManagerFactory;
    IncomeHibernateControl incomeHibernateControl;
    Gson gson;

    {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Income.class, new IncomeJsonSerializer());
        gsonBuilder.registerTypeAdapter(Income.class, new IncomeJsonSerializer());
        gsonBuilder.registerTypeAdapter(Category.class, new CategoryJsonSerializer());
        gsonBuilder.registerTypeAdapter(Company.class, new CompanyJsonSerializer());
        gsonBuilder.registerTypeAdapter(Person.class, new PersonJsonSerializer());

        this.gson = gsonBuilder.create();
        this.entityManagerFactory = Persistence.createEntityManagerFactory(Constants.SCHEMA_NAME);
        this.incomeHibernateControl = new IncomeHibernateControl(entityManagerFactory);
    }

    @RequestMapping(value = "income/getTotalIncomeByDate", method = RequestMethod.GET)
    @ResponseBody
    public String getTotalIncomeByDate(String fromDate, String toDate) {
        LocalDate localDateFromDate;
        LocalDate localDateToDate;

        try {
            localDateFromDate = (fromDate == null || "".equals(fromDate)) ? null : LocalDate.parse(fromDate);
        } catch (DateTimeParseException exception) {
            return "Failed while parsing fromDate";
        }

        try {
            localDateToDate = (toDate == null || "".equals(toDate)) ? null : LocalDate.parse(toDate);
        } catch (DateTimeParseException exception) {
            return "Failed while parsing toDate";
        }

        double totalIncome = incomeHibernateControl.getTotalIncomeByDate(localDateFromDate, localDateToDate);
        return gson.toJson(totalIncome);
    }

    @RequestMapping(value = "income/getAllIncomesOfCategory", method = RequestMethod.GET)
    @ResponseBody
    public String getAllIncomesOfCategory(Long categoryId){
        List<Income> incomes = incomeHibernateControl.getAllIncomesOfCategory(categoryId);
        return gson.toJson(incomes);
    }

    @RequestMapping(value = "income/getIncomeById", method = RequestMethod.GET)
    @ResponseBody
    public String getIncomeById(Long id){
        Income income = incomeHibernateControl.getIncomeById(id);
        return gson.toJson(income);
    }

    @RequestMapping(value = "income/updateIncome", method = RequestMethod.PUT)
    @ResponseBody
    public String updateIncome(@RequestBody String request){
        Gson parser = new Gson();
        Properties data = parser.fromJson(request, Properties.class);
        String id = data.getProperty("incomeId");
        String name = data.getProperty("incomeName");
        String amount = data.getProperty("incomeAmount");
        Income income = incomeHibernateControl.getIncomeById(Long.parseLong(id));
        income.setName(name);
        income.setAmount(Double.parseDouble(amount));
        incomeHibernateControl.edit(income);
        return "Income updated";
    }

    @RequestMapping(value = "income/deleteIncome", method = RequestMethod.DELETE)
    @ResponseBody
    public String deleteIncome(@RequestBody String request){
        Properties data = new Gson().fromJson(request, Properties.class);
        Long id = Long.parseLong(data.getProperty("id"));
        if (incomeHibernateControl.getIncomeById(id) == null) {
            return "Income doesn't exist";
        }
        incomeHibernateControl.remove(id);
        return "Income deleted";
    }

    @RequestMapping(value = "income/createIncome", method = RequestMethod.POST)
    @ResponseBody
    public String createIncome(@RequestBody String request){
        Properties data = new Gson().fromJson(request, Properties.class);
        Long categoryId = Long.parseLong(data.getProperty("categoryId"));

        CategoryHibernateControl categoryHibernateControl = new CategoryHibernateControl(entityManagerFactory);
        if (categoryHibernateControl.getCategoryById(categoryId) == null) {
            return "Category doesn't exist. Income creation failed.";
        }

        String incomeName = data.getProperty("incomeName");
        Double incomeAmount = Double.parseDouble(data.getProperty("incomeAmount"));

        Income income = new Income(incomeName, incomeAmount);
        incomeHibernateControl.createIncome(income, categoryId);
        return "Income created";
    }
}
