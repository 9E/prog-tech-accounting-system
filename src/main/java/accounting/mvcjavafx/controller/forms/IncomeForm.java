package accounting.mvcjavafx.controller.forms;

import accounting.mvcjavafx.controller.FormType;
import accounting.mvcjavafx.model.Category;
import accounting.mvcjavafx.model.User;
import accounting.mvcjavafx.model.accsys.AccountingSystem;
import accounting.mvcjavafx.model.moneyflow.Income;
import accounting.mvcjavafx.model.moneyflow.MoneyFlow;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import static accounting.mvcjavafx.controller.ControllerUtil.closeActionEventsStage;

public class IncomeForm {
    public Label labelUnderCreator;
    public TextField nameField;
    public TextField amountField;

    private User loggedInUser;
    private Category incomeCategory;
    private Income incomeToEdit;
    private FormType formType;
    private ListView<Income> incomeListView;

    private EntityManagerFactory entityManagerFactory;

    public void initializeForEdit(EntityManagerFactory entityManagerFactory, User loggedInUser, Category category,
                                  Income incomeToEdit, ListView<Income> incomeListView) {
        this.entityManagerFactory = entityManagerFactory;
        this.loggedInUser = loggedInUser;
        this.incomeCategory = category;
        this.incomeToEdit = incomeToEdit;
        this.incomeListView = incomeListView;
        this.formType = figureOutFormType(incomeToEdit);
        this.formType = FormType.EDIT;

        this.labelUnderCreator.setText(loggedInUser.getUsername());
        this.nameField.setText(incomeToEdit.getName());
        this.amountField.setText(Double.toString(incomeToEdit.getAmount()));
    }

    public void initializeForCreate(EntityManagerFactory entityManagerFactory, User loggedInUser,
                                    Category category, ListView<Income> incomeListView) {
        this.entityManagerFactory = entityManagerFactory;
        this.loggedInUser = loggedInUser;
        this.incomeCategory = category;
        this.incomeToEdit = null;
        this.incomeListView = incomeListView;
        this.formType = FormType.CREATE;
        
        this.labelUnderCreator.setText(loggedInUser.getUsername());
    }

    private FormType figureOutFormType(MoneyFlow moneyFlow) {
        if (moneyFlow == null) {
            return FormType.CREATE;
        }
        return FormType.EDIT;
    }

    public void performConfirm(ActionEvent actionEvent) {

        if (!isDouble(amountField.getText())) {
            new Alert(Alert.AlertType.ERROR, "Money amount is incorrect").show();
            return;
        }

        double amount = Double.parseDouble(amountField.getText());
        String incomeName = nameField.getText();

        switch (formType) {
            case CREATE -> {
                EntityManager entityManager = entityManagerFactory.createEntityManager();
                entityManager.getTransaction().begin();

                Category category = entityManager.find(incomeCategory.getClass(), incomeCategory.getId());
                User user = entityManager.find(loggedInUser.getClass(), loggedInUser.getId());
                Income income = new Income(incomeName, user, amount, category);
                category.addIncome(income);

                entityManager.getTransaction().commit();
                entityManager.close();

                incomeCategory.addIncome(income);
                incomeListView.getItems().add(income);
            }
            case EDIT -> {
                EntityManager entityManager = entityManagerFactory.createEntityManager();
                entityManager.getTransaction().begin();

                Income income = entityManager.find(Income.class, incomeToEdit.getId());
                income.setAmount(amount);
                income.setName(incomeName);

                entityManager.getTransaction().commit();
                entityManager.close();

                incomeToEdit.setAmount(amount);
                incomeToEdit.setName(incomeName);
                incomeListView.refresh();
            }
        }
        closeActionEventsStage(actionEvent);
    }

    public boolean isDouble(String doubleString) {
        try {
            Double.parseDouble(doubleString);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public User getLoggedInUser() {
        return loggedInUser;
    }

    public void setLoggedInUser(User loggedInUser) {
        this.loggedInUser = loggedInUser;
    }

    public Category getIncomeCategory() {
        return incomeCategory;
    }

    public void setIncomeCategory(Category incomeCategory) {
        this.incomeCategory = incomeCategory;
    }

    public Income getIncomeToEdit() {
        return incomeToEdit;
    }

    public void setIncomeToEdit(Income incomeToEdit) {
        this.incomeToEdit = incomeToEdit;
    }
}
