package accounting.mvcjavafx.controller.forms;

import accounting.mvcjavafx.controller.FormType;
import accounting.mvcjavafx.hibernate.CategoryHibernateControl;
import accounting.mvcjavafx.hibernate.HibernateControl;
import accounting.mvcjavafx.hibernate.UserHibernateControl;
import accounting.mvcjavafx.model.Category;
import accounting.mvcjavafx.model.User;
import javafx.event.ActionEvent;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import static accounting.mvcjavafx.controller.ControllerUtil.closeActionEventsStage;

public class CategoryFormWindow {
    public Label labelUnderCreator;
    public Label labelUnderParentCategory;
    public TextField categoryNameField;

    private User loggedInUser;
    private TreeItem<Category> categoryItem;
    private FormType formType;

    private EntityManagerFactory entityManagerFactory;
    private UserHibernateControl userHibernateControl;
    private CategoryHibernateControl categoryHibernateControl;

    public void customInitialize(EntityManagerFactory entityManagerFactory, User loggedInUser, TreeItem<Category> categoryItem, FormType formType) {
        this.loggedInUser = loggedInUser;
        this.categoryItem = categoryItem;
        this.formType = formType;

        labelUnderCreator.setText(loggedInUser.getUsername());

        Category category = categoryItem.getValue();

        if (formType == FormType.EDIT) {
            categoryNameField.setText(category.getName());
        }

        if (category.hasCreator() && category.hasParentCategory()) {
            labelUnderParentCategory.setText("id = " + categoryItem.getValue().getParentCategory().getId());
        }

        this.entityManagerFactory = entityManagerFactory;
        this.userHibernateControl = new UserHibernateControl(entityManagerFactory);
        this.categoryHibernateControl = new CategoryHibernateControl(entityManagerFactory);
    }

    public void performConfirm(ActionEvent actionEvent) {
        Category category = categoryItem.getValue();

        switch (formType) {
            case CREATE -> {
                TreeItem<Category> newCategoryItem;
                if (!category.hasCreator() && !category.hasParentCategory()) {
                    EntityManager entityManager = entityManagerFactory.createEntityManager();
                    entityManager.getTransaction().begin();

                    User user = entityManager.find(User.class, loggedInUser.getId());
                    Category parentCategory = new Category(user, null, categoryNameField.getText());
                    user.addParentCategory(parentCategory);

                    entityManager.getTransaction().commit();
                    entityManager.close();

                    newCategoryItem = new TreeItem<>(parentCategory);
                } else {
                    EntityManager entityManager = entityManagerFactory.createEntityManager();
                    entityManager.getTransaction().begin();

                    User user = entityManager.find(User.class, loggedInUser.getId());
                    Category parentCategory = entityManager.find(category.getClass(), category.getId());
                    Category subcategory = new Category(user, parentCategory, categoryNameField.getText());
                    parentCategory.addSubcategory(subcategory);

                    entityManager.getTransaction().commit();
                    entityManager.close();

                    newCategoryItem = new TreeItem<>(subcategory);
                }

                categoryItem.getChildren().add(newCategoryItem);
            }
            case EDIT -> {
                category.setName(categoryNameField.getText());
                categoryHibernateControl.edit(category);
                categoryItem.setValue(category);
                refreshItemInGUI(categoryItem);
            }
        }
        closeActionEventsStage(actionEvent);
    }

    public void refreshItemInGUI(TreeItem<Category> treeItem) {
        if (treeItem.getParent() != null) {
            List<TreeItem<Category>> parentsChildren = treeItem.getParent().getChildren();
            int categoryItemsIndex = parentsChildren.indexOf(treeItem);
            parentsChildren.set(categoryItemsIndex, treeItem);
        }
    }

    public TextField getCategoryNameField() {
        return categoryNameField;
    }

    public void setCategoryNameField(TextField categoryNameField) {
        this.categoryNameField = categoryNameField;
    }

    public User getLoggedInUser() {
        return loggedInUser;
    }

    public void setLoggedInUser(User loggedInUser) {
        this.loggedInUser = loggedInUser;
    }
}
