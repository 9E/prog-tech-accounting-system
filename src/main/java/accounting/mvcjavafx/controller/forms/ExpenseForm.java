package accounting.mvcjavafx.controller.forms;

import accounting.mvcjavafx.controller.FormType;
import accounting.mvcjavafx.hibernate.CategoryHibernateControl;
import accounting.mvcjavafx.hibernate.ExpenseHibernateControl;
import accounting.mvcjavafx.hibernate.UserHibernateControl;
import accounting.mvcjavafx.model.Category;
import accounting.mvcjavafx.model.User;
import accounting.mvcjavafx.model.accsys.AccountingSystem;
import accounting.mvcjavafx.model.moneyflow.Expense;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import static accounting.mvcjavafx.controller.ControllerUtil.closeActionEventsStage;

public class ExpenseForm {
    public Label labelUnderCreator;
    public TextField nameField;
    public TextField amountField;
    public TextField receiptNumberField;

    private AccountingSystem accountingSystem;
    private User loggedInUser;
    private Category expenseCategory;
    private Expense expenseToEdit;
    private FormType formType;
    private ListView<Expense> expenseListView;

    private EntityManagerFactory entityManagerFactory;

    public void initializeForEdit(EntityManagerFactory entityManagerFactory, User loggedInUser, Category category,
                                  Expense expenseToEdit, ListView<Expense> expenseListView) {
        this.entityManagerFactory = entityManagerFactory;
        this.loggedInUser = loggedInUser;
        this.expenseCategory = category;
        this.expenseToEdit = expenseToEdit;
        this.expenseListView = expenseListView;
        this.formType = FormType.EDIT;

        this.labelUnderCreator.setText(loggedInUser.getUsername());
        this.nameField.setText(expenseToEdit.getName());
        this.amountField.setText(Double.toString(expenseToEdit.getAmount()));
        this.receiptNumberField.setText((expenseToEdit).getReceiptNumber());
    }

    public void initializeForCreate(AccountingSystem accountingSystem, User loggedInUser,
                                    Category category, ListView<Expense> expenseListView) {
        this.accountingSystem = accountingSystem;
        this.loggedInUser = loggedInUser;
        this.expenseCategory = category;
        this.expenseToEdit = null;
        this.expenseListView = expenseListView;
        this.formType = FormType.CREATE;

        this.labelUnderCreator.setText(loggedInUser.getUsername());
    }

    public void initializeForCreate(EntityManagerFactory entityManagerFactory, User loggedInUser,
                                    Category category, ListView<Expense> expenseListView) {
        this.entityManagerFactory = entityManagerFactory;
        this.loggedInUser = loggedInUser;
        this.expenseCategory = category;
        this.expenseToEdit = null;
        this.expenseListView = expenseListView;
        this.formType = FormType.CREATE;

        this.labelUnderCreator.setText(loggedInUser.getUsername());
    }

    public void performConfirm(ActionEvent actionEvent) {

        if (!isDouble(amountField.getText())) {
            new Alert(Alert.AlertType.ERROR, "Money amount is incorrect").show();
            return;
        }

        double inputAmount = Double.parseDouble(amountField.getText());
        String inputExpenseName = nameField.getText();
        String inputReceiptNumber = receiptNumberField.getText();

        ExpenseHibernateControl expenseHibernateControl = new ExpenseHibernateControl(entityManagerFactory);

        switch (formType) {
            case CREATE -> {
                Expense expense = new Expense(inputExpenseName, null, inputAmount, inputReceiptNumber, null);
                expenseHibernateControl.createExpense(expense, expenseCategory.getId());

                expenseListView.getItems().add(expense);
            }
            case EDIT -> {
                expenseToEdit.setAmount(inputAmount);
                expenseToEdit.setName(inputExpenseName);
                expenseToEdit.setReceiptNumber(inputReceiptNumber);
                expenseHibernateControl.editExpense(expenseToEdit);

                expenseListView.refresh();
            }
        }
        closeActionEventsStage(actionEvent);
    }

    public boolean isDouble(String doubleString) {
        try {
            Double.parseDouble(doubleString);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public AccountingSystem getAccountingSystem() {
        return accountingSystem;
    }

    public void setAccountingSystem(AccountingSystem accountingSystem) {
        this.accountingSystem = accountingSystem;
    }

    public User getLoggedInUser() {
        return loggedInUser;
    }

    public void setLoggedInUser(User loggedInUser) {
        this.loggedInUser = loggedInUser;
    }

    public Category getExpenseCategory() {
        return expenseCategory;
    }

    public void setExpenseCategory(Category expenseCategory) {
        this.expenseCategory = expenseCategory;
    }

    public Expense getExpenseToEdit() {
        return expenseToEdit;
    }

    public void setExpenseToEdit(Expense expenseToEdit) {
        this.expenseToEdit = expenseToEdit;
    }

    public FormType getFormType() {
        return formType;
    }

    public void setFormType(FormType formType) {
        this.formType = formType;
    }

    public ListView<Expense> getExpenseListView() {
        return expenseListView;
    }

    public void setExpenseListView(ListView<Expense> expenseListView) {
        this.expenseListView = expenseListView;
    }
}
