package accounting.mvcjavafx.controller;

import accounting.mvcjavafx.model.Category;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;

import java.util.ArrayList;
import java.util.List;

public class CategoryInfoWindow {
    public TextField creatorField;
    public TextField parentCategoryField;
    public TextField categoryNameField;
    public TextField incomeWithoutSubcategoriesField;
    public TextField expensesWithoutSubcategoriesField;
    public TextArea supervisorsField;
    public TextField balanceWithoutSubcategoriesField;
    public TextField incomeWithSubcategoriesField;
    public TextField expensesWithSubcategoriesField;
    public TextField balanceWithSubcategoriesField;

    public void customInitialize(TreeItem<Category> categoryTreeItem) {
        Category category = categoryTreeItem.getValue(); // TODO: check whether I need to fetch it from the database

        String creatorFieldText = "";
        if (category.hasCreator()) {
            creatorFieldText = category.getCreator().getUsername();
        }
        this.creatorField.setText(creatorFieldText);

        String parentCategoryFieldText = "";
        if (category.hasParentCategory()) {
            parentCategoryFieldText = category.getParentCategory().getName();
        }
        this.parentCategoryField.setText(parentCategoryFieldText);

        this.categoryNameField.setText(category.getName());
        this.incomeWithoutSubcategoriesField.setText(Double.toString(category.getSumOfIncomes()));
        this.expensesWithoutSubcategoriesField.setText(Double.toString(category.getSumOfExpenses()));
        this.balanceWithoutSubcategoriesField.setText(Double.toString(category.getBalance()));

        this.incomeWithSubcategoriesField.setText(Double.toString(category.getSumOfIncomesWithSubcategories()));
        this.expensesWithSubcategoriesField.setText(Double.toString(category.getSumOfExpensesWithSubcategories()));
        this.balanceWithSubcategoriesField.setText(Double.toString(category.getBalanceWithSubcategories()));

        List<String> usernames = new ArrayList<>();
        category.getSupervisors().forEach(supervisor -> usernames.add(supervisor.getUsername()));

        this.supervisorsField.setText(usernames.toString());
    }
}
