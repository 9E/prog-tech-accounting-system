package accounting.mvcjavafx.controller;

import accounting.mvcjavafx.hibernate.AccountingSystemHibernateControl;
import accounting.mvcjavafx.hibernate.UserHibernateControl;
import accounting.mvcjavafx.model.Constants;
import accounting.mvcjavafx.model.accsys.AccountingSystem;
import accounting.mvcjavafx.model.accsys.VanillaAccountingSystem;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.IOException;

public class LoginWindow {
    public Button logInButton;
    public Button signUpButton;
    public TextField usernameField;
    public PasswordField passwordField;


    private final EntityManagerFactory entityManagerFactory;
    private final UserHibernateControl userHibernateControl;
    private final AccountingSystemHibernateControl accountingSystemHibernateControl;

    {
        this.entityManagerFactory = Persistence.createEntityManagerFactory(Constants.SCHEMA_NAME);
        this.userHibernateControl = new UserHibernateControl(entityManagerFactory);
        this.accountingSystemHibernateControl = new AccountingSystemHibernateControl(entityManagerFactory);

        // TODO: should probably move this part to "Start.java"
        AccountingSystem accountingSystem = accountingSystemHibernateControl.getAccountingSystemById(1L);
        if (accountingSystem == null) { // TODO: note, if an item with id = 1 already existed and was deleted, this won't create accountingSystem with id = 1
            accountingSystem = new VanillaAccountingSystem("NameSystemAccounting", "0.99");
            accountingSystemHibernateControl.create(accountingSystem);
        }
    }

    private final AccountingSystem accountingSystem = accountingSystemHibernateControl.getAccountingSystemById(Constants.DEFAULT_ACC_SYS_ID);

    public void performLogIn(ActionEvent actionEvent) throws IOException {
        String username = usernameField.getText();
        String password = passwordField.getText();
        if (!userHibernateControl.isLoginInfoCorrect(username, password)) {
            new Alert(Alert.AlertType.ERROR, "Username or password is incorrect").show();
        } else {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(("../view/MainMenuWindow.fxml")));
            Parent root = loader.load();

            MainMenuWindow mainMenuWindow = loader.getController();
//            mainMenuWindow.setAccountingSystem(accountingSystem);
            mainMenuWindow.setLoggedInUser(userHibernateControl.getUserByUsername(username));
            mainMenuWindow.customInitialize(entityManagerFactory);

            Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            stage.setScene(new Scene(root));
        }
    }

    public void performSignUp(ActionEvent actionEvent) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(("../view/SignUpWindow.fxml")));
        Parent root = loader.load();

        SignUpWindow signUpWindow = loader.getController();
        signUpWindow.setEntityManagerFactory(entityManagerFactory);

        Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        stage.setScene(new Scene(root));
    }

    public void performAccountingSystemInfo(ActionEvent actionEvent) {

        new Alert(Alert.AlertType.INFORMATION, String.format("System name: %s\n" +
                "System version: %s\n" +
                "Creation date: %s",
                accountingSystem.getName(),
                accountingSystem.getVersion(),
                accountingSystem.getCreationDate())).showAndWait();
    }
}
