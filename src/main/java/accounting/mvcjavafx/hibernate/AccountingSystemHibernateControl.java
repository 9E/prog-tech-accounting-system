package accounting.mvcjavafx.hibernate;

import accounting.mvcjavafx.model.User;
import accounting.mvcjavafx.model.accsys.AccountingSystem;

import javax.persistence.EntityManagerFactory;
import java.util.List;

public class AccountingSystemHibernateControl extends HibernateControl{

    public AccountingSystemHibernateControl(EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory, AccountingSystem.class);
    }
    
    public void create(Object object) {
        executeInTransaction(() -> entityManager.persist(object));
    }

    public void edit(Object object) {
        executeInTransaction(() -> entityManager.merge(object));
    }

    public <T> void remove(T id) {
        executeInTransaction(() -> {
            Object objectToRemove = entityManager.find(getObjectClass(), id);

            if (objectToRemove != null) {
                entityManager.remove(objectToRemove);
            }
        });
    }

    public <T> AccountingSystem getAccountingSystemById(T id) {
        return (AccountingSystem) getObjectById(id);
    }

    public List<AccountingSystemHibernateControl> getAllAccountingSystems() {
        return getAllObjects();
    }
}
