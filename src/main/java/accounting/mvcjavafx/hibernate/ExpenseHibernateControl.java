package accounting.mvcjavafx.hibernate;

import accounting.mvcjavafx.model.Category;
import accounting.mvcjavafx.model.User;
import accounting.mvcjavafx.model.moneyflow.Expense;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

public class ExpenseHibernateControl extends HibernateControl {

    public ExpenseHibernateControl(EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory, Expense.class);
    }

    public void remove(Long id) {
        executeInTransaction(() -> {
            Expense expense = (Expense) entityManager.find(getObjectClass(), id);
            Category owningCategory = expense.getCategory();
            owningCategory.getExpenses().remove(expense);
        });
    }

    public Expense getExpenseById(Long id) {
        return (Expense) getObjectById(id);
    }

    public List<Expense> getAllExpensesOfCategory(Long categoryId) {
        List<Expense> expenses = getAllExpenses();
        expenses.removeIf(expense -> !Objects.equals(expense.getCategory().getId(), categoryId));
        return expenses;
    }

    public List<Expense> getAllExpenses() {
        return getAllObjects();
    }

    public void createExpense(Expense expense, Long expenseCategoryId) {
        executeInTransaction(() -> {
            Category category = entityManager.find(Category.class, expenseCategoryId);
            User user = entityManager.find(category.getCreator().getClass(), category.getCreator().getId());
            expense.setCreator(user);
            expense.setCategory(category);
            category.addExpense(expense);
        });
    }

    public void editExpense(Expense expenseToEdit) {
        executeInTransaction(() -> {
            Expense expense = entityManager.find(expenseToEdit.getClass(), expenseToEdit.getId());
            expense.setAmount(expenseToEdit.getAmount());
            expense.setName(expenseToEdit.getName());
            expense.setReceiptNumber(expense.getReceiptNumber());
        });
    }

    public double getTotalExpensesByDate(LocalDate fromDate, LocalDate toDate) {
        LocalDate comparedFromDate = fromDate == null ? LocalDate.MIN : fromDate;
        LocalDate comparedToDate = toDate == null ? LocalDate.MAX : toDate;

        System.out.println(fromDate);
        System.out.println(toDate);

        List<Expense> expenses = getAllExpenses();

        expenses.removeIf(expense -> expense.getCreationDate().isBefore(comparedFromDate) ||
                expense.getCreationDate().isAfter(comparedToDate));

        Category tempCategory = new Category();
        tempCategory.setExpenses(expenses);

        return tempCategory.getSumOfExpensesWithSubcategories();
    }
}
