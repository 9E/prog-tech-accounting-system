package accounting.mvcjavafx.gson;

import accounting.mvcjavafx.model.moneyflow.Income;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

public class IncomeJsonSerializer implements JsonSerializer<Income> {

    @Override
    public JsonElement serialize(Income income, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", income.getId());
        jsonObject.addProperty("name", income.getName());
        jsonObject.addProperty("amount", income.getAmount());
        jsonObject.addProperty("creationDate", income.getCreationDate().toString());
        return jsonObject;
    }
}
