package accounting.mvcjavafx.model.moneyflow;

import accounting.mvcjavafx.model.Category;
import accounting.mvcjavafx.model.User;

import javax.persistence.Entity;

@Entity
public class Expense extends MoneyFlow{
    private String receiptNumber;

    public Expense(String name, User createdBy, double amount, String receiptNumber) {
        super(name, createdBy, amount);
        this.receiptNumber = receiptNumber;
    }

    public Expense(String name, User creator, double amount, String receiptNumber, Category category) {
        super(name, creator, amount, category);
        this.receiptNumber = receiptNumber;
    }

    public Expense(String name, double amount, String receiptNumber) {
        super(name, amount);
        this.receiptNumber = receiptNumber;
    }

    public Expense() {
    }

    public String getReceiptNumber() {
        return receiptNumber;
    }

    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    @Override
    public String toString() {
        String creatorUsername = getCreator() == null ? "" : getCreator().getUsername();
        return "Expense{" +
                "id=" + getId() +
                ", name='" + getName() + '\'' +
                ", amount=" + getAmount() +
                ", creator='" + creatorUsername +
                "',receiptNumber='" + receiptNumber + '\'' +
                ", creationDate=" + getCreationDate() +
                '}';
    }
}
