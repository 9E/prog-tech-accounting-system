package accounting.mvcjavafx.model.moneyflow;

import accounting.mvcjavafx.model.Category;
import accounting.mvcjavafx.model.User;

import javax.persistence.Entity;

@Entity
public class Income extends MoneyFlow {

    public Income(String name, User createdBy, double amount) {
        super(name, createdBy, amount);
    }

    public Income(String name, User creator, double amount, Category category) {
        super(name, creator, amount, category);
    }
    public Income(String name, double amount) {
        super(name, amount);
    }

    public Income() {
    }

    @Override
    public String toString() {
        String creatorUsername = getCreator() == null ? "" : getCreator().getUsername();
        return "Income{" +
                "id=" + getId() +
                ", name='" + getName() + '\'' +
                ", amount=" + getAmount() +
                ", creator=" + creatorUsername +
                ", creationDate=" + getCreationDate() +
                ", category=" + getCategory() +
                '}';
    }
}
