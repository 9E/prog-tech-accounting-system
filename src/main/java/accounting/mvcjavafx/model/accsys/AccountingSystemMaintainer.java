package accounting.mvcjavafx.model.accsys;

import java.io.*;

public class AccountingSystemMaintainer {
    private static final String DATA_FILE = "accountingSystem.lib";

    public static void saveAccountingSystem(AccountingSystem accountingSystem) {
        try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(DATA_FILE))){
            outputStream.writeObject(accountingSystem);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static AccountingSystem loadAccountingSystem() {
        AccountingSystem accountingSystem;
        try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(DATA_FILE))){
            accountingSystem = (AccountingSystem) inputStream.readObject();
        } catch (ClassNotFoundException e) {
            System.out.println("Couldn't recognize the class");
            return newAccoutningSystemInitialization();
        } catch (IOException e) {
            System.out.println("Failed to open data file");
            return newAccoutningSystemInitialization();
        }
        return accountingSystem;
    }

    private static AccountingSystem newAccoutningSystemInitialization() {
        System.out.println("Creating new accounting system");
        String name = "AccSys";
        String version = "1.0";

        return new VanillaAccountingSystem(name, version);
    }
}
