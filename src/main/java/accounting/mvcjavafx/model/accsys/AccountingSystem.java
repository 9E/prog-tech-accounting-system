package accounting.mvcjavafx.model.accsys;

import accounting.mvcjavafx.model.Category;
import accounting.mvcjavafx.model.User;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
public abstract class AccountingSystem implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String version;
    @OneToMany(mappedBy = "accountingSystem", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Category> systemRootCategories;
    @OneToMany(mappedBy = "accountingSystem", cascade = CascadeType.ALL, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<User> systemUsers;
    private LocalDate creationDate;

    public AccountingSystem(String name, String version, LocalDate creationDate) {
        this.name = name;
        this.version = version;
        this.systemRootCategories = new ArrayList<>();
        this.systemUsers = new ArrayList<>();
        this.creationDate = creationDate;
    }

    public AccountingSystem(String name, String version) {
        this.name = name;
        this.version = version;
        this.systemRootCategories = new ArrayList<>();
        this.systemUsers = new ArrayList<>();
        this.creationDate = LocalDate.now();
    }

    public AccountingSystem() {

    }

    abstract public User getUserByUsername(String username);

    abstract public <T> User getUserById(T id);

    abstract public boolean isUsernameTaken(String username);

    abstract public boolean isLoginInfoCorrect(String username, String password);

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public List<Category> getSystemRootCategories() {
        return systemRootCategories;
    }

    public void setSystemRootCategories(List<Category> systemRootCategories) {
        this.systemRootCategories = systemRootCategories;
    }

    public List<User> getSystemUsers() {
        return systemUsers;
    }

    public void setSystemUsers(List<User> systemUsers) {
        this.systemUsers = systemUsers;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public String toString() {
        return "AccountingSystem{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", version='" + version + '\'' +
                ", creationDate=" + creationDate +
                '}';
    }
}
