package accounting.mvcjavafx.model;

import accounting.mvcjavafx.model.accsys.AccountingSystem;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
public abstract class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String username;
    private String password;
    @OneToOne(cascade = CascadeType.ALL)
    ContactInfo contactInfo;
    @ManyToMany(mappedBy = "supervisors", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Category> supervisedCategories;
    private LocalDate creationDate;

    @ManyToOne
    private AccountingSystem accountingSystem;

    @OneToMany(mappedBy = "creator", cascade = CascadeType.ALL)
    private List<Category> createdCategories;

    public User(String username, String password, ContactInfo contactInfo, AccountingSystem accountingSystem) {
        this.username = username;
        this.password = password;
        this.contactInfo = contactInfo;
        this.supervisedCategories = new ArrayList<>();
        this.creationDate = LocalDate.now();
        this.accountingSystem = accountingSystem;
        this.createdCategories = new ArrayList<>();
    }

    public User(String username, String password, ContactInfo contactInfo) {
        this.username = username;
        this.password = password;
        this.contactInfo = contactInfo;
        this.supervisedCategories = new ArrayList<>();
        this.creationDate = LocalDate.now();
        this.createdCategories = new ArrayList<>();
    }

    public User() {

    }

    public void assignCategoryTo(User user, Category category) {
        user.getSupervisedCategories().add(category);
        category.assignSupervisorToSubcategoriesRecursively(user);
    }

    public void addParentCategory(String categoryName) {
        Category category = new Category(this, null, categoryName);
        category.setAccountingSystem(this.accountingSystem);
        this.supervisedCategories.add(category);
    }

    public void addParentCategory(Category category) {
        category.setAccountingSystem(this.accountingSystem);
        this.supervisedCategories.add(category);
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public List<Category> getSupervisedCategories() {
        return supervisedCategories;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ContactInfo getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(ContactInfo contactInfo) {
        this.contactInfo = contactInfo;
    }

    public void setSupervisedCategories(List<Category> supervisedCategories) {
        this.supervisedCategories = supervisedCategories;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public AccountingSystem getAccountingSystem() {
        return accountingSystem;
    }

    public void setAccountingSystem(AccountingSystem accountingSystem) {
        this.accountingSystem = accountingSystem;
    }

    public List<Category> getCreatedCategories() {
        return createdCategories;
    }

    public void setCreatedCategories(List<Category> createdCategories) {
        this.createdCategories = createdCategories;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", contactInfo=" + contactInfo +
                ", supervisedCategories=" + supervisedCategories +
                ", creationDate=" + creationDate +
                ", accountingSystem=" + accountingSystem +
                '}';
    }
}
