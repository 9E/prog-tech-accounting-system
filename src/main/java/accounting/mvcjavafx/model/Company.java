package accounting.mvcjavafx.model;

import accounting.mvcjavafx.model.accsys.AccountingSystem;

import javax.persistence.Entity;

@Entity
public class Company extends User{
    private String companyName;

    public Company(String username, String password, ContactInfo contactInfo, String companyName,
                   AccountingSystem accountingSystem) {
        super(username, password, contactInfo, accountingSystem);
        this.companyName = companyName;
    }

    public Company(String username, String password, ContactInfo contactInfo, String companyName) {
        super(username, password, contactInfo);
        this.companyName = companyName;
    }

    public Company(String companyName) {
        this.companyName = companyName;
    }

    public Company() {
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String name) {
        this.companyName = name;
    }
}
