package accounting.commandlineui;

import accounting.mvcjavafx.model.User;
import accounting.mvcjavafx.model.accsys.AccountingSystem;
import accounting.mvcjavafx.model.accsys.AccountingSystemMaintainer;

import java.util.Scanner;

public class CommandLineMenu {
    private AccountingSystem accountingSystem;
    private Scanner scanner = new Scanner(System.in);
    private Registration registration;
    private MainMenu mainMenu;

    public CommandLineMenu(AccountingSystem accountingSystem) {
        this.accountingSystem = accountingSystem;
        this.registration = new Registration(this, accountingSystem);
        this.mainMenu = new MainMenu(this, accountingSystem);
    }

    public CommandLineMenu() {
        this.accountingSystem = AccountingSystemMaintainer.loadAccountingSystem();
        this.registration = new Registration(this, this.accountingSystem);
        this.mainMenu = new MainMenu(this, accountingSystem);
    }

    public void welcomePrompt() {
        boolean exitState = false;

        while(!exitState) {
            System.out.println("Welcome to the accounting system, what would you like to do?");
            System.out.println("1. Login");
            System.out.println("2. Create a new account");
            System.out.println("3. Exit\n");

            String input = scanner.nextLine();

            switch (input) {
                case "1":
                    loginPrompt();
                    break;
                case "2":
                    registration.createNewAccountPrompt();
                    break;
                case "3":
                    if (exitState = wantToExit()) {
                        AccountingSystemMaintainer.saveAccountingSystem(this.accountingSystem);
                        System.out.println("Goodbye!");
                        System.exit(0);
                    }
                    break;
                default:
                    System.out.println("Wrong choice, please try again.");
            }
        }
    }

    public void loginPrompt() {
        boolean exitState = false;

        while(!exitState) {
            System.out.println("Please enter your username:");
            String username = this.scanner.nextLine();
            System.out.println("Please enter your password:");
            String password = this.scanner.nextLine();

            if (!this.accountingSystem.isUsernameTaken(username)) {
                System.out.println("Such username doesn't exist.");
            } else if (!this.accountingSystem.isLoginInfoCorrect(username, password)) {
                System.out.println("Incorrect password.");
            } else {
                User user = accountingSystem.getUserByUsername(username);
                mainMenu.setCurrentUser(user);
                System.out.println("You have succesfully logged in.");
                this.mainMenu.mainMenuPrompt();
                break;
            }

            exitState = returnToWelcomeMenu();
        }
    }


    public boolean wantToExit() {
        System.out.println("Do you want to exit? (y/n)");
        String input = this.scanner.nextLine().trim().toLowerCase();

        switch (input) {
            case "y":
                return true;
            case "n":
                return false;
            default:
                System.out.println("Wrong choice, please type again.");
                return returnToWelcomeMenu();
        }
    }

    public boolean returnToWelcomeMenu() {
        System.out.println("Do you want to return to welcome menu? (y/n)");
        String input = this.scanner.nextLine().trim().toLowerCase();

        switch (input) {
            case "y":
                return true;
            case "n":
                return false;
            default:
                System.out.println("Wrong choice, please type again.");
                return returnToWelcomeMenu();
        }
    }
}
